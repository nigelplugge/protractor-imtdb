import {ElementFinder} from 'protractor';

export class WebdriverTools {

    static async getClasses(element: ElementFinder): Promise<string[]> {
        return (await element.getAttribute('class')).split(' ').map(item => item.trim());
    }

}