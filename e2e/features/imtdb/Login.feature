# language: nl
@e2e @homepage
Functionaliteit: Login - De inlog functionaliteit van de website

  Achtergrond: de gebruiker navigeert naar de homepage
    Gegeven dat de gebruiker op de homepage is

  Scenario: Een gebruiker kan inloggen met de juiste credentials
    Als de gebruiker op de 'Login' knop klikt
    En de gebruiker de volgende credentials verstuurd
      | gebruikersnaam | wachtwoord |
      | test           | test       |
    Dan is de gebruiker ingelogd

  Scenario: Een gebruiker kan niet inloggen met onjuiste credentials
    Als de gebruiker op de 'Login' knop klikt
    En de gebruiker de volgende credentials verstuurd
      | gebruikersnaam | wachtwoord |
      | fgdfg          | dfgdf      |
    Dan verschijnt de melding Login Failed

