# language: nl
@e2e @homepage
Functionaliteit: Homepagina - De pagina waar de gebruiker op land bij het navigeren naar de IMTDB website

  Achtergrond: de gebruiker navigeert naar de homepage
    Gegeven dat de gebruiker op de homepage is

  Scenario: Op de homepage is een 'Coming soon' trailer aanwezig
    Dan is er een youtube trailer aanwezig

  Scenario: De 'List all movies' knop leidt naar de all movies pagina
    Als de gebruiker op de 'List all movies' knop klikt
    Dan verschijnen er 34 filmtitels op de 'all movies' pagina

