import {setDefaultTimeout, Then, When} from 'cucumber';
import {Moviepage} from '../page_objects/Moviepage.po';
import {browser, ExpectedConditions, protractor} from 'protractor';
import {Homepage} from "../page_objects/Homepage.po";

const chai = require('chai');
const chaiString = require('chai-string');
const chaiSmoothie = require('chai-smoothie');
const chaiUrl = require('chai-url');

chai.use(chaiSmoothie)
    .use(chaiString)
    .use(chaiUrl);

const expect = chai.expect;

const moviepage = new Moviepage();
const homepage = new Homepage();

setDefaultTimeout(60 * 1000);


Then(/verschijnen er ([^"]*) filmtitels op de 'all movies' pagina$/, async (aantal) => {
    await expect(await moviepage.movieList()).to.be.lengthOf(parseInt(aantal));
});
