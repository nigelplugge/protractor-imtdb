import {Given, When, Then, setDefaultTimeout} from 'cucumber';
import {browser, ExpectedConditions, protractor} from 'protractor';
import {Homepage} from '../page_objects/Homepage.po';
import {Moviepage} from '../page_objects/Moviepage.po';

const chai = require('chai');
const chaiString = require('chai-string');
const chaiSmoothie = require('chai-smoothie');

chai.use(chaiSmoothie)
    .use(chaiString);
const expect = chai.expect;
const homepage = new Homepage();
const contentpage = new Moviepage();

setDefaultTimeout(60 * 1000);
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = '0';


Given(/^dat de gebruiker op de homepage is$/, async () => {
    await browser.get(browser.baseUrl);
});

When(/^de gebruiker op de '([^"]*)' knop klikt$/, async text => {
    await browser.wait(ExpectedConditions.elementToBeClickable(homepage.buttonText(text)), 5000);
    await homepage.buttonText(text).click()
});

Then(/^is er een youtube trailer aanwezig$/, async () => {
    await expect(homepage.trailer).to.be.displayed;
    await expect(await homepage.trailer.getAttribute('src')).to.includes('https://www.youtube.com/embed/');
});
