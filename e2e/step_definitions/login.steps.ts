import {After, Given, Then, When} from 'cucumber';
import {LoginPage} from '../page_objects/LoginPage.po';
import {Homepage} from '../page_objects/Homepage.po';
import {$$, browser, by, ElementFinder, ExpectedConditions} from 'protractor';

const chai = require('chai');
const chaiString = require('chai-string');
const chaiSmoothie = require('chai-smoothie');

chai.use(chaiSmoothie)
    .use(chaiString);
const expect = chai.expect;
const loginpage = new LoginPage();
const homepage = new Homepage();


When(/^de gebruiker de volgende credentials verstuurd$/, async (credentials) => {
    for (const data of credentials.hashes()) {
        loginpage.username.sendKeys(data.gebruikersnaam);
        loginpage.password.sendKeys(data.wachtwoord);
    }
    loginpage.submit.click();
});

Then(/^is de gebruiker ingelogd$/, async () => {
    await browser.sleep(5000);
    await expect(homepage.buttonText('Logout')).to.be.displayed;
});

Then(/^verschijnt de melding ([^"]*)$/, async (melding) => {
    await browser.wait(ExpectedConditions.presenceOf(await homepage.buttonText(melding)), 5000);
    await expect(homepage.buttonText(melding)).to.be.displayed;
});
