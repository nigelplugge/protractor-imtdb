import {$, by, element, ElementFinder} from 'protractor';
import {browser} from 'protractor';

export class Homepage  {

    homepage: ElementFinder;
    trailer: ElementFinder;

    constructor() {
        this.homepage = $('body');
        this.trailer = $('.sneek-preview iframe')
    }

    buttonText (text) {
        return element.all(by.xpath(`//*[text()='${text}']`)).first();
    }
}