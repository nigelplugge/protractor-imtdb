import {$, browser, by, element, ElementArrayFinder, ElementFinder, until} from 'protractor';
import {Homepage} from './Homepage.po';


export class Moviepage extends Homepage {

    body: ElementFinder;
    contentpage: ElementFinder;

    constructor() {
        super();
        this.contentpage = $('content');
        this.body = $('body');
    }

    movieList(){
        return element.all(by.css('main.movies ul li'));
    }

}

