import {$, $$, by, element, ElementArrayFinder, ElementFinder} from 'protractor';

const chai = require('chai');
const chaiString = require('chai-string');
const chaiSmoothie = require('chai-smoothie');

chai.use(chaiSmoothie)
    .use(chaiString);
const expect = chai.expect;

export class LoginPage {

    body: ElementFinder;
    username: ElementFinder;
    password: ElementFinder;
    submit: ElementFinder;

    constructor() {
        this.body = $('body');
        this.username = $('#username');
        this.password = $('#password');
        this.submit = $('button.login__submit');
    }

}