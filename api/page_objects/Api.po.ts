import {$, $$, browser, by, element, ElementArrayFinder, ElementFinder} from 'protractor';
import {Given} from "cucumber";
import {Moviepage} from "../../e2e/page_objects/Moviepage.po";

const request = require('request');

export class Api {
    async request(service, parameterType, parameters, type) {
        let url = '';
        let fullParameter = '';
        switch (service) {
            case 'user':
                url = browser.userUrl;
                break;
            case 'movie':
                url = browser.movieUrl;
                break;
        }
        if (url == ''){
            url = service
        }
        switch (parameterType) {
            case 'query':
                url += '?';
                for (const parameter of parameters.hashes()) {
                    fullParameter += parameter.key + '=' + parameter.value + '&';
                }
                fullParameter = fullParameter.slice(0, -1);
                break;
            case 'url':
                for (const parameter of parameters.hashes()) {
                    fullParameter += parameter.key+'/';
                }
                fullParameter = fullParameter.slice(0, -1);
                break;
            case 'url en query':
                for (const parameter of parameters.hashes()) {
                    fullParameter += parameter.urlParam+'/';
                }
                fullParameter = fullParameter.slice(0, -1);
                fullParameter += '?';
                for (const parameter of parameters.hashes()) {
                    if(parameter.queryKey != '') {
                        fullParameter += parameter.queryKey + '=' + parameter.queryValue + '&';
                    }
                }
                fullParameter = fullParameter.slice(0, -1);
                break;
            case 'raw':
                for (const parameter of parameters.hashes()) {
                    fullParameter = parameter.key
                }
                break;
        }
        browser.result = undefined;
        browser.serviceResponse = undefined;
        let newHeaders = {};
        let fullUrl = url + fullParameter;
        console.log(fullUrl);
        if (browser.headers != undefined) {
            for (const header of browser.headers.hashes()) {
                newHeaders[header.key] = header.value;
            }}
        const options = {
            url: fullUrl,
            method: type,
            headers: newHeaders,
        };

        request(options, async function(err, res, body) {
            browser.result = res;
            browser.serviceResponse = body;
        });
        while(0 < 1){
            await browser.sleep(300);
            if (browser.result != undefined && browser.serviceResponse != undefined){
                console.log('Request done');
                break;
            }
        }
        if(this.isJson(browser.serviceResponse)) {
            browser.json = JSON.parse(browser.serviceResponse);
            // console.log(browser.json);
        }
        // console.log(browser.serviceResponse);
    }

    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }


    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}