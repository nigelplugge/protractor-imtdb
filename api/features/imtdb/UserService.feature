# language: nl
@api @services @user
Functionaliteit: API - User service

  Scenario: De user database bevat minimaal 3 users
    Gegeven er een GET request wordt gedaan naar de user service met de volgende url parameters
      | key   |
      | api   |
      | Users |
    Dan is de status code 200
    En zijn er minimaal 3 users in de service


