# language: nl
@api @services @movie
Functionaliteit: API - Movie service

  Abstract Scenario: Informatie over '<titel>>' kan worden opgehaald
    Gegeven er een GET request wordt gedaan naar de movie service met de volgende url parameters
      | key    |
      | api    |
      | movies |
      | <id>   |
    Dan is de status code 200
    En is de response film titel '<title>'
    Voorbeelden:
      | id        | title                        |
      | tt0064116 | Once Upon a Time in the West |
