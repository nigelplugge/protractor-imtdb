import {setDefaultTimeout, Given, Then, When} from 'cucumber';
import {Api} from '../page_objects/Api.po';
import {browser} from 'protractor';

const chai = require('chai');
const chaiString = require('chai-string');
const chaiSmoothie = require('chai-smoothie');
const chaiUrl = require('chai-url');
const chaiHttp = require('chai-http');

chai.use(chaiSmoothie)
    .use(chaiString)
    .use(chaiUrl)
    .use(chaiHttp);

const expect = chai.expect;
const api = new Api();

setDefaultTimeout(60 * 1000);

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = '0';

Given(/^er een ([^"]*) request wordt gedaan naar de (user|movie) service met de volgende (url|query|raw|url en query) parameters$/, async(type, service, parameterType, parameters) => {
    browser.parameterList.push(parameters);
    browser.service = service;
    await api.request(service, parameterType, parameters, type);
});


Given(/^er een request wordt gedaan naar ([^"]*) in de ([^"]*) omgeving$/, async(bestand, omgeving, parameters) => {
    let parameterType = 'url';
    let type = 'GET';
    browser.parameterList.push(parameters);
    await api.request(omgeving, parameterType, parameters, type);
});

Given(/^er een URL request wordt gedaan naar ([^"]*)$/, async(url) => {
    let parameterType = 'none';
    let type = 'GET';
    let parameters = '';
    await api.request(url, parameterType, parameters, type);
});

Given(/^de volgende headers worden meegegeven$/, async(headers) => {
    browser.headers = headers;
});

Then(/^is de status code ([^"]*)$/, async(status) => {
    await expect(browser.result.statusCode).to.equal(parseInt(status));
});

Then(/^zijn er minimaal ([^"]*) users in de service$/, async(aantalUsers) => {
    let calc = browser.json.allUsers.length >= aantalUsers;
    await expect(calc).to.true;
});

Then(/^is de response film titel '([^"]*)'$/, async(title) => {
    await expect(browser.json.title).to.equal(title);
});

