import {Before, After} from 'cucumber';
import {browser, by} from 'protractor';
import * as fs from 'fs';

Before(async function Omgevingen() {
    //Opzetten omgeving voor de API tests
    browser.userUrl = 'http://localhost:4242/';
    browser.movieUrl = 'http://localhost:4243/';

    browser.parameterList = [];
    browser.responseList = [];
    browser.headers = undefined;
});