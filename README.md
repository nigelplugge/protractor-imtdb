# Testen voor de IMTDB website

## Installatie
Hier staan de stappen beschreven om het framework te installeren.

Stap 1:
Clone de repository: https://nigelplugge@bitbucket.org/nigelplugge/protractor-imtdb.git

Stap 2: 
Voer het commando 'npm install' uit in de root folder van het project


## Draaien tests
De scripts om de testen de draaien staan in de package.json. Om bijvoorbeeld de end-to-end testen te draaien voer je het commando 'npm run test:e2e' uit.
Om de API testen te draaien voer je het commando 'npm run test:api' uit.

## Het rapport
Na het draaien van de tests vind je in de folder reports de rapporten terug van de tests. Na het draaien van de tests wordt in deze folder een nieuwe folder aangemaakt met e2e of api waarin de rapport bestanden worden opgeslagen. Door het bestand index.html te openen kun je het rapport weergeven. 